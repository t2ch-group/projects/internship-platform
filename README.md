# Internship Platform

Платформа T2CH для организации стажировок

# Требования
1. NodeJS v10.0.0^ (npm v5.6.0^)

# Настройка проекта
1. Склонировать репозиторий на диск:
```bash
git clone https://gitlab.com/t2ch-group/projects/internship-platform
```
2. Перейти в папку с клиентом VueJS и установить npm зависимости:
```bash
cd client/
npm install
```
3. `*` Установить vue-cli для упрощения дальнейшей разработки на VueJS:
```bash
npm install -g @vue/cli
```

# Запуск проекта
```bash
cd client/
npm run serve
```

# Сборка проекта
```bash
cd client/
npm run build
```

`*` Не обязательно