import Vue from 'vue';
import VueRouter from 'vue-router';
import Rating from "./components/Rating";
import Projects from "./components/Projects";
import Profile from "./components/Profile";

Vue.use(VueRouter);

export default new VueRouter({
    mode: 'history',
    routes: [
        {path: '/', component: Rating},
        {path: '/projects', component: Projects},
        {path: '/profile/:id', component: Profile, props: true},
    ]
});